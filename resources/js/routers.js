import Vue from 'vue/dist/vue'
import VueRouter from "vue-router";
Vue.use(VueRouter);

import Dashboard from './components/Dashboard'
import PageHead from './components/layouts/Header'

const routes = [
    {
        path: '/',
        redirect: '/dashboard',
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        components: {
            default: Dashboard
        }
    }
]

const router = new VueRouter({
    mode: "history",
    routes,
});


// router.beforeEach((to, from, next) => {
//     const publicPages = ['/login', '/register'];
//     const authRequired = !publicPages.includes(to.path);
//     const loggedIn = localStorage.getItem('login_token');
//
//     if (authRequired && !loggedIn) {
//         return next('/login');
//     }
//     next();
// })

// router.beforeEach((to, from, next) => {
//     const token  = window.localStorage.getItem('login_token');
//     if (!token && to.meta.requiresAuth){
//         next({ name: 'Login' })
//     }
//     else next()
// })

export default router;
