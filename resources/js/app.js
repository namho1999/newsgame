require('./bootstrap');
import Vue from 'vue/dist/vue'
window.Vue = Vue;
import router from "./routers";
import Antd from "ant-design-vue";
import App from "./App";
Vue.use(Antd);

Vue.component("uploadimages", require("./components/Upload.vue"));

const app = new Vue({
    el: '#app',
    render: h => h(App),
    router
});
