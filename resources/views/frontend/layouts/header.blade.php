<div id="wp-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header">
                    <div id="logo">
                        <a href="">
                            <img src="https://gamek.mediacdn.vn/web_images/gamek_logo_30052022.svg" alt="">
                        </a>
                    </div>
                    <div class="menu-header">
                        <nav>
                            <ul class="list-menu">
                                <li><a href="">Game Mobile</a></li>
                                <li><a href="">Esport</a></li>
                                <li><a href="">Video game</a></li>
                                <li><a href="">Cộng đồng</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div id="wp-cart-network">
                        <div id="cart">
                            <i class="fa-solid fa-cart-plus"></i>
                        </div>
                        <div class="list-network">
                            <a href=""><i class="fa-brands fa-facebook"></i></a>
                            <a href=""><i class="fa-brands fa-youtube"></i></a>
                            <a href=""><i class="fa-brands fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        window.onscroll = function () {
            myFunction()
        };
        var header = document.getElementById("wp-header");
        var sticky = header.offsetTop;

        function myFunction() {
            if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
            } else {
                header.classList.remove("sticky");
            }
        }
    </script>
@endpush

