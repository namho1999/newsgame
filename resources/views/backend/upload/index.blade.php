@extends('frontend.layouts.main')
@section('content')
    <div class="page-content-wrapper">
        <div id="app">
           <uploadimages></uploadimages>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('/js/app.js') }}"></script>
@endpush
