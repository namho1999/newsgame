@extends('layouts.auth')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="/"><b>Trang quản trị</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg" style="font-size: 1.75rem;
    font-weight: 600;
    line-height: 1.2;color: #181c32">Đăng nhập</p>
            <form method="POST" action="{{route('postLogin')}}">
                @csrf
                <label style="color: #181c32" class="form-label fs-6 fw-bolder text-dark">Số điện thoại</label>
                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} has-feedback">
                    <input style="background-color: #f5f8fa;
                            border-color: #f5f8fa;
                            color: #5e6278;
                            min-height: calc(1.5em + 1.65rem + 2px );
                            padding: 0.825rem 1.5rem ;
                            border-radius: 0.475rem ;
                            transition: color .2s ease,background-color .2s ease;
" type="text" name="phone" class="form-control" placeholder="Số điện thoại"
                           value="{{ old('phone') }}" autofocus>
                    @if ($errors->has('phone'))
                        <span class="help-block" style="color: red">
                        {{ $errors->first('phone') }}
                    </span>
                    @endif
                </div>

                <label style="color: #181c32" class="form-label fw-bolder text-dark fs-6 mb-0">Mật khẩu</label>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                    <input style="background-color: #f5f8fa;
                            border-color: #f5f8fa;
                            color: #5e6278;
                            min-height: calc(1.5em + 1.65rem + 2px );
                            padding: 0.825rem 1.5rem ;
                            border-radius: 0.475rem ;
                            transition: color .2s ease,background-color .2s ease;
                            " type="password" name="password" class="form-control"
                           placeholder="Mật khẩu">
                    @if ($errors->has('password'))
                        <span class="help-block" style="color: red">
                        {{ $errors->first('password') }}
                    </span>
                    @endif
                </div>

                <div class="row">
                    {{--                    <div class="col-xs-8">--}}
                    {{--                        <div class="checkbox icheck">--}}
                    {{--                            <label>--}}
                    {{--                                <input type="checkbox"--}}
                    {{--                                       name="remember" {{ old('remember') ? 'checked' : '' }}> {{ trans('auth.remember_me') }}--}}
                    {{--                            </label>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="submit"
                                class="btn btn-primary btn-block btn-flat" style="color: #fff;
    border-color: #009ef7;
    background-color: #009ef7;
    font-size: 17px;
    height: 45px;
    margin-top: 20px;
    border-radius: 0.475rem;">Đăng nhập
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <!-- <div class="social-auth-links text-center">
      <p>- HOẶC -</p>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> {{ trans('auth.login_with_google') }}</a>
    </div> -->
            <!-- /.social-auth-links -->

            {{--<a href="{{ route('password.request') }}">{{ trans('auth.forgot_password') }}</a><br>--}}
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
@endsection
