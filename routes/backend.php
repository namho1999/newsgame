<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'Admin::', 'namespace' => 'Admin'], function () {
    Route::get('/{any}', 'AdminController@index')->where('any', '.*');
});
