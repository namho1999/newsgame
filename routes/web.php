<?php

use Illuminate\Support\Facades\Route;
Route::group(['namespace' => 'Frontend'], function () {
    Route::get('/', 'IndexController@index')->name('home');
});

Route::group(['namespace' => 'Backend'], function () {
    Route::get('/upload', 'IndexController@upload')->name('upload');
});

Route::get('login', 'AuthWeb\AuthController@getLogin')->name('getLogin');
Route::post('login', 'AuthWeb\AuthController@postLogin')->name('postLogin');
Route::get('logout', 'AuthWeb\AuthController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::view('/{any}', 'home')
    ->where('any', '.*');


