<?php

namespace App\Http\Controllers\AuthWeb;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;

class AuthController extends Controller
{
    function getLogin() {
        return view('auth.login');
    }

    function postLogin(Request $request) {
        $request->validate(
            [
                'phone' => 'required|string|max:255',
                'password' => 'required|string|max:255'
            ],
            [
                'required' => ':attribute không được để trống',
                'max' => ':attribute tối đa 255 ký tự',
            ],
            [
                'phone' => '* Số điện thoại',
                'password' => '* Mật khẩu'
            ]
        );

        $phone = $request->input('phone');
        $password = $request->input('password');

        if( Auth::attempt(['phone' => $phone, 'password' =>$password])) {
            $user = auth()->user();
            $user->last_login = Carbon::now();
            $user->last_login_ip = $request->ip();
            $user->session_id = $request->session()->getId();
            $user->save();
            Session::put('user_id', $user->id);
            return redirect('/admin/dashboard');
        } else {
            return redirect()->back()->with('status', 'Tài khoản học mật khẩu không chính xác');
        }
    }

    function logout() {
        Session::flush();
        Session::regenerate();
        Auth::logout();
        return redirect('login');
    }
}
